#  -------------------------------------------------------------------------  #
#   _____                  _             _  _  _                              #
#  | ____| _ __ __      __(_) _ __      | |(_)| |_  ___  _   _                #
#  |  _|  | '__|\ \ /\ / /| || '_ \  _  | || || __|/ __|| | | |               #
#  | |___ | |    \ V  V / | || | | || |_| || || |_ \__ \| |_| |               #
#  |_____||_|     \_/\_/  |_||_| |_| \___/ |_| \__||___/ \__,_|               #
#                                                                             # 
#  FILE: i3/config                                                            # 
#  Creator: Erwin                                                             #
#  -------------------------------------------------------------------------  #

# Globals
set $mod Mod4
set $terminal alacritty

#---------------------APPLICATIONS---------------------#

# Software keybindings
bindsym $mod+Return exec $terminal
bindsym $mod+Shift+d --release exec "killall dunst; exec notify-send 'restart dunst'"
bindsym Print exec --no-startup-id i3-scrot
bindsym $mod+Print --release exec --no-startup-id i3-scrot -w
bindsym $mod+Shift+Print --release exec --no-startup-id i3-scrot -s
bindsym $mod+Ctrl+x --release exec --no-startup-id xkill
bindsym $mod+p exec keepassxc
bindsym $mod+w exec --no-startup-id dmenu_run -fn 'Ubuntu Condensed-12' # Dmenu - F1
bindsym $mod+b exec brave							# Browser - F2
bindsym $mod+v exec $terminal -e vifmrun 			# Vifm    - F3 
bindsym $mod+Shift+v exec pcmanfm 					# Vifm3d  - S+F3 
bindsym $mod+c exec $terminal -e cmus 				# Cmus 	  - F4 
bindsym $mod+d exec discord							# Discord - F5 
bindsym $mod+s exec steam							# Steam	  - F6 

# Alternative Function keys
bindsym $mod+F1 exec --no-startup-id dmenu_run -fn 'Ubuntu Condensed-12' 
bindsym $mod+F2 exec brave 
bindsym $mod+F3 exec $terminal -e vifmrun
bindsym $mod+Shift+F3 exec pcmanfm 
bindsym $mod+F4 exec $terminal -e cmus
bindsym $mod+F5 exec discord 
bindsym $mod+F6 exec steam 

#--- Autostart/Autoexec ---#
exec_always --no-startup-id $HOME/.config/polybar/launch.sh
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec_always --no-startup-id feh -zr --bg-fill $HOME/.local/share/backgrounds
exec --no-startup-id nm-applet
exec_always --no-startup-id picom --config .config/picom/picon.conf &
# Uncomment for login sound
# TODO: This is not perfect, consider using a script
#exec --no-startup-id ffplay -fs -nodisp -autoexit ~/.config/i3/loginsound.mp3
#--- Autostart/Autoexec ---#

#---------------------APPLICATIONS---------------------#


# Window rules
new_window pixel 1
new_float normal
title_align center
hide_edge_borders none
font pango:Ubuntu Mono Bold 14
focus_follows_mouse no

# i3-gaps only 
gaps inner 6
gaps outer -3
smart_borders on

# Use Mouse+$mod to drag floating windows
floating_modifier $mod

# Kill focused window
bindsym $mod+Shift+q kill

# Audio controls
# Alsa 
#bindsym XF86AudioMute exec amixer sset 'Master' toggle
#bindsym XF86AudioLowerVolume exec amixer sset 'Master' 5%-
#bindsym XF86AudioRaiseVolume exec amixer sset 'Master' 5%+
# Pulse 
#bindsym XF86AudioRaiseVolume exec --no-startup-id pactl -- set-sink-volume @DEFAULT_SINK@ +5% #increase sound volume
#bindsym XF86AudioLowerVolume exec --no-startup-id pactl -- set-sink-volume @DEFAULT_SINK@ -5% #decrease sound volume
#bindsym XF86AudioMute exec --no-startup-id pactl -- set-sink-mute @DEFAULT_SINK@ toggle # mute sound


# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# split orientation
bindsym $mod+q split toggle

# toggle fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# Workspace names
set $ws1 1
set $ws2 2
set $ws3 3
set $ws4 4
set $ws5 5
set $ws6 6
set $ws7 7
set $ws8 8

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8

# Move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8

# Move to workspace with focused container
bindsym $mod+Ctrl+1 move container to workspace $ws1; workspace $ws1
bindsym $mod+Ctrl+2 move container to workspace $ws2; workspace $ws2
bindsym $mod+Ctrl+3 move container to workspace $ws3; workspace $ws3
bindsym $mod+Ctrl+4 move container to workspace $ws4; workspace $ws4
bindsym $mod+Ctrl+5 move container to workspace $ws5; workspace $ws5
bindsym $mod+Ctrl+6 move container to workspace $ws6; workspace $ws6
bindsym $mod+Ctrl+7 move container to workspace $ws7; workspace $ws7
bindsym $mod+Ctrl+8 move container to workspace $ws8; workspace $ws8

# Open applications on specific workspaces
assign [class="Brave"] $ws1

# Open specific applications in floating mode
for_window [class="Lxappearance"] floating enable border normal
for_window [class="Pavucontrol"] floating enable

# switch to workspace with urgent window automatically
for_window [urgent=latest] focus

# reload the configuration file
bindsym $mod+Shift+r reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Ctrl+r restart

# reload polybar
bindsym $mod+Shift+p exec --no-startup-id $HOME/.config/polybar/launch.sh

# change background
bindsym $mod+Shift+b exec --no-startup-id feh -zr --bg-fill $HOME/.local/share/backgrounds

# Set shut down, restart and locking features
bindsym $mod+0 mode "$mode_system"
set $mode_system (l)ogout, (u)ser, (r)eboot, (Shift+s)hutdown
mode "$mode_system" {
    bindsym l exec --no-startup-id i3exit logout, mode "default"
    bindsym r exec --no-startup-id i3exit reboot, mode "default"
    bindsym Shift+s exec --no-startup-id i3exit shutdown, mode "default"

    # exit system mode: "Enter" or "Escape"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Resize window (you can also use the mouse for that)
bindsym $mod+r mode "resize"
mode "resize" {
        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 4 px or 4 ppt
        bindsym j resize grow height 4 px or 4 ppt
        bindsym k resize shrink height 4 px or 4 ppt
        bindsym l resize grow width 4 px or 4 ppt

        # exit resize mode: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}
