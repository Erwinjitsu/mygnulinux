local mason_ok, mason = pcall(require, "mason")
if not mason_ok then
  return
end

local mason_lspconfig_ok, mason_lspconfig = pcall(require, "mason-lspconfig")
if not mason_lspconfig_ok then
  return
end

local lspconfig_ok, lspconfig = pcall(require, "lspconfig")
if not lspconfig_ok then
  return
end

-- Setup the plugins
mason.setup({
  ui = {
    border = "rounded"
  }
})
mason_lspconfig.setup({
  ensure_installed = { "clangd" },
})
lspconfig.lua_ls.setup{}
lspconfig.clangd.setup{}

